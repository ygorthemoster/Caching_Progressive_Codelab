# Google Codelab: Caching with progressive libraries Codelab
This is an implementation of a Google codelab tutorial, the tutorial is freely avaliable at: https://codelabs.developers.google.com/codelabs/using-caching/index.html, this tutorial teaches the use of [sw-precache](https://github.com/GoogleChrome/sw-precache) and [sw-toolbox](https://github.com/GoogleChrome/sw-toolbox) to create a progressive app, this code is the final product of said tutorial

## Pre-requisites
To test this project you'll need
- [git](https://git-scm.com/)
- A webserver as per tutorial I'll be using [web-server for chrome](https://chrome.google.com/webstore/detail/web-server-for-chrome/ofhbbkphhbklhfoeikjpcbhemlocgigb)

## Installing

All you have to do is clone this repository

```
git clone https://ygorthemoster@gitlab.com/ygorthemoster/Caching_Progressive_Codelab.git
```

## Running
You can view the app by accessing the hosting link provided by the webserver, if you are using web-server for chrome normally it'll be at: http://127.0.0.1:8887

## License
See [LICENSE](LICENSE)

## Acknowledgments
- Original source by [Google Chrome](https://github.com/GoogleChrome) available at: https://github.com/GoogleChrome/samples/blob/gh-pages/fetch-api/fetch-reddit-demo.js
- Tutorial by [Google Codelabs](https://codelabs.developers.google.com/) available at: https://codelabs.developers.google.com/codelabs/using-caching/index.html
